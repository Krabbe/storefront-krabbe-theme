# Storefront Krabbe Theme

The StoreFront Krabbe Theme is my personal starter child theme for WooThemes StoreFront WooCommerce theme. It is based on Storefront Child Theme by Stuart Duff.

## Usage

This child theme is designed to be used as a starter theme for the WooCommerce StoreFront theme which you can download for free below.

* [Download WooCommerce StoreFront Theme](https://wordpress.org/themes/storefront/)
* [StoreFront Documentation](http://docs.woocommerce.com/documentation/themes/storefront/)
* [StoreFront Child Themes](https://woocommerce.com/product-category/themes/storefront-child-theme-themes/)
* [StoreFront Extensions](https://woocommerce.com/product-category/storefront-extensions/)

Custom PHP that you write should be added to the child themes functions.php file whilst any custom CSS should be added to the child themes style.css file.

There is also a style.scss file within the /assets/sass/ folder that can be used if you wish to write [SASS - Syntactically Awesome Style Sheets](http://sass-lang.com/) based styles which can then be compiled into the style.css file using an app like [CodeKit](https://incident57.com/codekit/) for OSX or [PrePros](https://prepros.io/) for Windows.
